// logical operators with boolean
// highIncome, CIBILsCore -> loan Eligible, Inligible

let highIncome = true; 
let CIBILSCore = false;

//Add &&
//let eligibileperson = highIncome && CIBILSCore;

//OR  || 
//let eligibileperson = highIncome || CIBILSCore;

//NOT !!
let eligibileperson = highIncome || CIBILSCore;

let applicationStatus =! eligibileperson;

console.log("Loan status:" + eligibileperson);
console.log("Application status:" + applicationStatus);

//logical with non- boolean

let userColor= "green"; // string
let defaultColor= "blue"; // string
let currentColor= userColor || defaultColor;

console.log("selected color: " + currentColor);

//false (false)
// undefined
// null
// 0
//false
// '' or ""
//NaN

// truty -> Anything this not falsy is -> Truthy

// true || false = true
// false || true = false
// true || "dinesh" = dinesh (check with boolean mechanism)
// false || 0 = 0 (checks breakdown)