//let x = {}; // let x = new object(){}
// let name = "dinesh"; //-> new string("dinesh");
// let age = 20; // numberliterals -> new number()
// let isAlive = true; //booleanliterals -> new boolean()

// console.log(name,age,isAlive);

let name = new String("dinesh");
let age =  new Number(20);
let isAlive = new Boolean(true);

console.log(name, age, isAlive);
