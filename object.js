//objects
let person = {
    name: "swetha",
    age: 19,
    isalive: true,
    gender: "female",
    address: "chennai, Tamil nadu, India",
    sibling: {brother: "sanjay", sister: "senha"}
};
// end object

//Dot Notation
console.log(person.sibling.sister);


// Bracket Notation
//console.log(person['gender'])

let person1 = {
    name: "dinesh",
    age: 20,
    gender: "male",
    address: "Chennai, Tamil nadu, India",
    sibling: {brother1:"durai", brother2:"ramesh"}
};
console.log(person1.sibling.brother1);