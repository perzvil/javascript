// primitive datatype(string,number,boolean,undefined,null,symbol)
// let x = 5;

// y=x;

// x=10;
// let cart = 5;
// function updateCart(cart){
//     cart ++;
// }
// updateCart(cart);
// console.log("carts available:" + cart);


// reference datatype(object,array and function)
// let x = {value:5};

// let y = x;

// x.value =20;

let cartobj = {value:5};
function updateCart(cart){
    cart.value ++;
}
updateCart(cartobj);
console.log( cartobj);