//Switch case basic syntax

//switch(condition){
//case 1:
// console.log("1");
//case 2:
// console.log("2");
//default:
//console.log("default behaviour");
//}

// S
// A
// B 
// E
// U
let mark = 30;

switch (true) {
    case mark > 90:
        console.log("super grade");
        break;
    case mark > 50:
        console.log("pass");
        break;
    case mark < 50:
        console.log("failed");
        break;
    default:
        console.log("unkown grade");
}

let amount = 20;

switch (true) {
    case amount > 10:
        console.log("buy a Android mobile");
        break;
    case amount > 60:
        console.log("buy a iphone mobile");
        break;
    case amount > 5:
        console.log("buy a basic mobile");
        break;
    default:
        console.log("go to home");
}
