// for(intialExpression; condition; step){
//     console.log("number #1");
// }

//For Loop

// for (let i = 10; i >= 1; i--){
//     if(i % 2 !== 0){
//         console.log("Odd number #" + i);
//     }
// }

//While Loop

// let i = 15;

// while(i >= 1){
//     if(i % 2 !== 0){
//         console.log("odd number #" + i);
//     }
//     i--;
// }

// do{
//     if(i % 2 !== 0){
//         console.log("odd number do while #" + i);
//     }
   
// }while (i <= 10);

//do-while
// let i = 7;
// while (i <= 13)
// {
//     if(i % 2 !== 0){
//         console.log("odd number" +1);
//     }
//     i ++ 
// }
// do{
//     if(i % 2 == 0){
//         console.log("even number" + i);
//     }
//     i++
// }while(i >= 10)

//for in loop
const person={
    name: 'dinesh',
    age : 20,
    sex : 'male'
}
for(let key in person)
{
    console.log("key : " + key);
}

const colors=['green','red','blue']
for(let value in colors){
    console.log(colors[value]);
}
for(let color of colors){
    console.log("color: " + color);
}