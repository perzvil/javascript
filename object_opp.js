// let name ="dinesh";
// let age = "20";
// let interest = ['Creating, teaching new things'];
// let address = {
//     city: "chennai",
//     state: "tamil nadu"
// }

// function greeting() {
//     let msg ="my name is " + name + ", I love " + interest ;
//     // let msg ='my name is ${name}, i love ${interest}';
//     console.log(msg);
// }
// greeting();

//factoryt function
function createperson(name){

    return {
        name : name,
        greeting() {
            let msg = "My name is " + this.name;
            console.log(msg);
        }
    };
}
let dinesh = createperson("dinesh");
let kumar = createperson("kumar");
dinesh.greeting();
kumar.greeting();

// Constructor functions 
function Person(name){  //pascal ->myFirstName
    this.name = name;
    this.greeting = function() {
        console.log('my name is ' + this.name);
    }

}
let person = new Person('dinesh');
let person1 = new Person('perzvil');
person.greeting();
person1.greeting();











