let x = 5;

// Equality operator
console.log(x === 5); //5 === 5
console.log( x !== 5); //5 !== 5
//Relational Operators
console.log(x < 5); // less than
console.log(x <= 5 ); //lessthan or equal too

console.log(x > 5); // greater than
console.log(x >= 5 ); // greaterthan or equal too

// string comparison operators
console.log('dinesh' > 'dina'); //dictionary


//comparison of different type
console.log('1' < 5);
console.log(true == 1); //1-true, 0-false

//strict equlity operator (data type + value)
console.log(1 === 1);
console.log('1' === 1);

// lose equlity operator 
console.log(1 == 1); //number + number
console.log( '1' == 1); //string + number
console.log( true == 1);  //boolean