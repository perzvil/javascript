//Adding element to JavaScript Array
// const count =["apple","orange","grapes"];

// //push
// count.push("lemon","strawberries")

// //unshift

// count.unshift("lemon","strawberries")

// //slice

// count.splice(3,0, "watermelons","bananas")

// console.log(count);

// Finding elements in Primitive datatypes
// const items =['apple','orange','grapes','apple']

// console.log(items.includes("Apple"));



// Finding elements in Reference  datatypes

// const orders = [
//     {id: 1, item: "smartphone", quantity: 1},
//     {id: 2, item: "bike", quantity: 1},
//     {id: 3, item: "biscuit", quantity: 5},
// ]

// let result = orders.findIndex(function(order){
//    return order.item === "smartphone"

// })
// console.log(result);

// let vegatables = [
//     {id:1 , item: "carrots", quantity: 5},
//     {id:2 , item: "beans", quantity: 10},
//     {id:1 , item: "onions", quantity: 15},
// ]
// // let result = vegatables.find(function(vegatables)
// // {
// //     return vegatables.item === 'beans';
// // }) 

// // Using Arrow functions
// let result = vegatables.find((order) => order.item === 'beans')
// console.log(result);



// Removing element in the array 
// const number = [1,2,3,4];

// //End 
// // Push => pop
// // number.pop()

// // Start
// // unshift => shift 
// // number.shift()

// //Middle
// //splice
// number.splice(2, 2)

// console.log(number);



// Emptying Array

// let number = [1,2,3,4]

// // solution 1 
// number = []

// // solution 2 
// // number.length = 0;

// // solution 3 
// // number.splice(0, number.length);

// //solution 4 
// // while(number.length) number.pop()


// console.log(number);


//combining Arrays
// const shopping_cart = [{item: "maagi"}];
// const additional_cart = ["beans",'masala',"matchbox",'salt'];

// // Combine
// let recipe = shopping_cart.concat(additional_cart);
// shopping_cart[0].item = "noodles"

// //spread operator
// let recipe =[...shopping_cart, ...additional_cart]

// // Extract 
// let extractedInfo = recipe.slice(1,3);

// console.log(recipe);
// console.log(extractedInfo);

//Literating elements
// const dailyRoutine = ['wake up','eat','sleep'];

// // For-of
// // for(let routine of dailyRoutine){
// //     //logics
// // console.log(routine);
// // }

// // // For-in 
// // for(let routine in dailyRoutine) {
// // console.log(routine, dailyRoutine [routine]);
// // }

// //for-each
// dailyRoutine.forEach(routine => console.log(routine));


// const dineshKumar = ['passion','lazy','blod'];

// //for-each
// dineshKumar.forEach(dinesh => console.log(dinesh));


//Joining and Splitting arrays

// const dailyRoutine = ['wake','eat','sleep'];

// let dailyRoutines = dailyRoutine.join(", ");

// console.log(dailyRoutine);

//splitting elements

// let fullname = "dinesh kumar";

// let userName = fullname.split(" ");

// let firstname = userName[0];
// let lastname = userName[1];
// console.log(`my first name is ${firstname} and last name is ${lastname}`);

let postTile = "This is My post!";

let post= postTile.toLowerCase().split(" ");

let finalpost =post.join("_")

console.log(finalpost);